#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>

int getBit( unsigned char number, int index )
{
  unsigned char tmp = 0x01;
  return (number >> index ) & tmp;
}

void drawPixel(Display *di, Window wi, GC gc, int x, int y, unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha){
    float source_A = ((float)alpha) /((float) 0xFF);
    red   = red   + (0xFF  * (1.0 - source_A));
    blue  = blue  + (0xFF  * (1.0 - source_A));
    green = green + (0xFF  * (1.0 - source_A));
    unsigned long color = (blue) | (green << 8) | (red << 16);
    XSetForeground(di, gc, color);
    XDrawPoint(di, wi, gc, x, y);
}

int drawBitmap(int rows, int width, int pitch, unsigned char* buffer, unsigned char pixel_mode){

    /* Open the display */
    Display *di = XOpenDisplay(getenv("DISPLAY"));
    if(di == NULL){
        printf("Couldn't open the display\n");
        return -1;
    }

    int sc = DefaultScreen(di);
    Window ro = DefaultRootWindow(di);
    Window wi = XCreateSimpleWindow(di, ro, 0, 0, width, rows, 1, WhitePixel(di, sc), WhitePixel(di, sc));
    XMapWindow(di, wi);
    XStoreName(di, wi, "Font Rendering A");

    /* Prepare GC window */
    GC gc = XCreateGC(di, ro, 0, NULL);

    /* Select what events the window listens to */
    XSelectInput(di, wi, KeyPressMask | ExposureMask);
    XEvent ev;

    int quit = 0;
    while(!quit){
        int a = XNextEvent(di, &ev);
        if(ev.type == KeyPress)
            quit = 1;
        if(ev.type == Expose){
            if ( pixel_mode == 7 )
            {
              int width_counter = 0;
              for(int i = 0; i < rows; i+=1){
                  width_counter = 0;
                  for(int j = 0; j < pitch; j+=1){
                      char blue = buffer[i*pitch + j];
                      j++;
                      char green = buffer[i*pitch + j];
                      j++;
                      char red = buffer[i*pitch + j];
                      j++;
                      char alpha = buffer[i*pitch + j];
                      drawPixel(di, wi, gc, width_counter, i, red, green, blue, alpha);
                      width_counter++;
                  }
              }
            }
            else if ( pixel_mode == 2 )
            {
              int width_counter = 0;
              for(int i = 0; i < rows; i+=1){
                  width_counter = 0;
                  for(int j = 0; j < pitch; j+=1){
                      char color = buffer[i*pitch + j];
                      drawPixel(di, wi, gc, width_counter, i, color, color, color, 0xFF);
                      width_counter++;
                  }
              }
            }
            else if ( pixel_mode == 1 )
            {
              int width_counter = 0;
              for(int i = 0; i < rows; i+=1){
                  width_counter = 0;
                  for(int j = 0; j < pitch; j+=1){
                      unsigned char color = buffer[i*pitch + j];
                      for ( int z = 7; z >= 0; z-- )
                      {
                        int res = getBit( color, z );
                        if ( res == 1 )
                        {
                          drawPixel(di, wi, gc, width_counter, i, 0xFF, 0xFF, 0xFF, 0xFF);
                        }
                        else
                        {
                          drawPixel(di, wi, gc, width_counter, i, 0x00, 0x00, 0x00, 0xFF);
                        }
                        width_counter++;
                      }
                  }
              }
            }
        }
    }
    XFreeGC(di, gc);
    XDestroyWindow(di, wi);
    XCloseDisplay(di);
}
