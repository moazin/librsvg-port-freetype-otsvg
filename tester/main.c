#include <rsvg_port.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include FT_OTSVG_H
#include FT_MODULE_H
#include <drawBitmap.h>

 int main(int argc, char* argv[])
 {
    if(argc != 4)
    {
      printf("invalid args\n");
      return -1;
    }
    FT_Library library;
    FT_Error error = FT_Err_Ok;
    error = FT_Init_FreeType(&library);
    if(error){
        printf("Error initializing the library");
        return 0;
    }
    FT_Face face;
    error = FT_New_Face(library, argv[1], 0, &face);
    if(error){
        printf("Error loading the font");
        return 0;
    }
    SVG_RendererHooks  hooks;
    hooks.init_svg        = (SVG_Lib_Init_Func)rsvg_port_init;
    hooks.free_svg        = (SVG_Lib_Free_Func)rsvg_port_free;
    hooks.render_svg      = (SVG_Lib_Render_Func)rsvg_port_render;
    hooks.preset_slot     = (SVG_Lib_Preset_Slot_Func)rsvg_port_preset_slot;
    FT_Property_Set( library, "ot-svg", "svg_hooks", &hooks );
    error = FT_Set_Pixel_Sizes(face, 1000, 1000);
    if(error){
        printf("Error setting the char size");
        return 0;
    }

    unsigned int glyph_index;
    if ( argv[2][0] == 'c' )
    {
      glyph_index = FT_Get_Char_Index( face, argv[3][0] );
    }
    else if ( argv[2][0] == 'i' )
    {
      sscanf( argv[3], "%d", &glyph_index );
    }

    error = FT_Load_Glyph(face, glyph_index, FT_LOAD_COLOR);
    if(error){
        printf("Error loading the glyph");
        return 0;
    }
    FT_Glyph  glyph_color;

    FT_Get_Glyph(face->glyph, &glyph_color);

    FT_Glyph_To_Bitmap(&glyph_color, FT_RENDER_MODE_NORMAL, NULL, FALSE);

    FT_BitmapGlyph bit_glyph_color = (FT_BitmapGlyph)glyph_color;
    drawBitmap(bit_glyph_color->bitmap.rows, bit_glyph_color->bitmap.width, bit_glyph_color->bitmap.pitch, bit_glyph_color->bitmap.buffer, bit_glyph_color->bitmap.pixel_mode);

    FT_Done_Face( face );
    FT_Done_FreeType( library );
   return 0;
 }
